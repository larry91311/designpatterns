﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuState : ISceneState
{
    public MainMenuState(SceneStateController Controller)//:base(Controller)
    {
        this.StateName="MainMenuState";
        base.m_Controller=Controller;
    }
    //開始
    public override void StateBegin()
    {
        Debug.Log("MainMenuState coming");
        Button tmpBtn=UITool.GetUIComponent<Button>("StartGameBtn");
        if(tmpBtn!=null)
        {
            tmpBtn.onClick.AddListener(()=>OnStartGameBtnClick(tmpBtn));
        }
    }
    public override void StateUpdate()
    {}

    public override void StateEnd()
    {}

    private void OnStartGameBtnClick(Button theButton)
    {
        //Debug.Log("OnStartBtnClick:"+theButton.gameObject.name);
        m_Controller.SetState(new BattleState(m_Controller),"BattleScene");
    }
}
