﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleState : ISceneState
{
    public BattleState(SceneStateController Controller)//:base(Controller)
    {
        this.StateName="BattleState";
        base.m_Controller=Controller;
    }
    //開始
    public override void StateBegin()
    {
        Debug.Log("Battle coming");
        //PBaseDefenseGame.Instance.Initial();
    }
    //更新
    public override void StateUpdate()
    {
        //輸入
        InputProcess();

        //遊戲邏輯
        //PBaseDefenseGame.Instance.Update();

        //Render由 Unity負責

        //遊戲是否結束
        // if(PBaseDefenseGame.Instance.ThisGameIsOver())
        // {
        //     m_Controller.SetState(new MainMenuState(m_Controller),"MainMenuState");
        // }
    }

    //輸入
    private void InputProcess()
    {
        //玩家輸入判斷程式碼....
    }
    public override void StateEnd()
    {
        //PBaseDefenseGame.Instance.Release();
    }

}
