﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartState : ISceneState
{
    public StartState(SceneStateController Controller)//:base(Controller)
    {
        this.StateName="StartState";
        base.m_Controller=Controller;
    }
    //開始
    public override void StateBegin()
    {
        //可在此進行遊戲資料載入及初始...等
        Debug.Log("StartState Begin");
    }
    //更新
    public override void StateUpdate()
    {
        //更換為
        m_Controller.SetState(new MainMenuState(m_Controller),"MainMenuScene");
        
    }
    public override void StateEnd()
    {}
}
