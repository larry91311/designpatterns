﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//整合子系統功能，提供一個高階介面讓客戶端使用，像微波爐一樣
public class PBaseDefenseGame : MonoBehaviour
{
    //------------------------------------------------------------------------
	// Singleton模版
	private static PBaseDefenseGame _instance;
	public static PBaseDefenseGame Instance
	{
		get
		{
			if (_instance == null)			
				_instance = new PBaseDefenseGame();
			return _instance;
		}
	}
    // // 場景狀態控制
	// private bool m_bGameOver = false;

    // //遊戲系統
	// private GameEventSystem m_GameEventSystem = null;	 // 遊戲事件系統
	// private CampSystem m_CampSystem	 = null; 			 // 兵營系統
	// private StageSystem m_StageSystem = null; 			 // 關卡系統
	// private CharacterSystem m_CharacterSystem = null; 	 // 角色管理系統
	// private APSystem m_ApSystem = null; 				 // 行動力系統
	// private AchievementSystem m_AchievementSystem = null;// 成就系統
	// // 界面
	// private CampInfoUI m_CampInfoUI = null;				 // 兵營界面
	// private SoldierInfoUI m_SoldierInfoUI = null;		 // 戰士資訊界面
	// private GameStateInfoUI m_GameStateInfoUI = null;	 // 遊戲狀態界面
	// private GamePauseUI m_GamePauseUI = null;			 // 遊戲暫停界面



    // // 初始P-BaseDefense遊戲相關設定
	// public void Initinal()
	// {
	// 	// 場景狀態控制
	// 	m_bGameOver = false;
	// 	// 遊戲系統
	// 	m_GameEventSystem = new GameEventSystem(this);	// 遊戲事件系統
	// 	m_CampSystem = new CampSystem(this);			// 兵營系統
	// 	m_StageSystem = new StageSystem(this);			// 關卡系統
	// 	m_CharacterSystem = new CharacterSystem(this); 	// 角色管理系統
	// 	m_ApSystem = new APSystem(this);				// 行動力系統
	// 	m_AchievementSystem = new AchievementSystem(this); // 成就系統
	// 	// 界面
	// 	m_CampInfoUI = new CampInfoUI(this); 			// 兵營資訊
	// 	m_SoldierInfoUI = new SoldierInfoUI(this); 		// Soldier資訊									
	// 	m_GameStateInfoUI = new GameStateInfoUI(this); 	// 遊戲資料
	// 	m_GamePauseUI = new GamePauseUI (this);			// 遊戲暫停

	// 	// // 注入到其它系統
	// 	// EnemyAI.SetStageSystem( m_StageSystem );

	// 	// // 載入存檔
	// 	// LoadData();

	// 	// // 註冊遊戲事件系統
	// 	// ResigerGameEvent();
	// }

    // public void Update()
	// {
	// 	// 玩家輸入
	// 	InputProcess();

	// 	// 遊戲系統更新
	// 	m_GameEventSystem.Update();
	// 	m_CampSystem.Update();
	// 	m_StageSystem.Update();
	// 	m_CharacterSystem.Update();
	// 	m_ApSystem.Update();
	// 	m_AchievementSystem.Update();

	// 	// 玩家界面更新
	// 	m_CampInfoUI.Update();
	// 	m_SoldierInfoUI.Update();
	// 	m_GameStateInfoUI.Update();
	// 	m_GamePauseUI.Update();
	// }

    // public void Release()
    // {
    //     // 遊戲系統
	// 	m_GameEventSystem.Release();
	// 	m_CampSystem.Release();
	// 	m_StageSystem.Release();
	// 	m_CharacterSystem.Release();
	// 	m_ApSystem.Release();
	// 	m_AchievementSystem.Release();
	// 	// 界面
	// 	m_CampInfoUI.Release();
	// 	m_SoldierInfoUI.Release();
	// 	m_GameStateInfoUI.Release();
	// 	m_GamePauseUI.Release();
	// 	UITool.ReleaseCanvas();

	// 	// 存檔
	// 	//SaveData();
    // }
    // // 遊戲狀態
	// public bool ThisGameIsOver()
	// {
	// 	return m_bGameOver;
	// }
    // // 目前敵人數量
	// public int GetEnemyCount()
	// {
	// 	if( m_CharacterSystem !=null)
	// 		return m_CharacterSystem.GetEnemyCount();
	// 	return 0;
	// }
    // //取得各單位數量
    // // public int GetUnitCount(ENUM_Soldier emSolider)
    // // {
    // //     return m_CharacterSystem.GetUnitCount(emSolider);
    // // }
    // // public int GetUnitCount(ENUM_Enemy emEnemy)
    // // {
    // //     return m_CharacterSystem.GetUnitCount(emEnemy);
    // // }



	// //升級Soldier                          //仲介者
	// public void UpgradeSoldier()
	// {
	// 	if(m_CharacterSystem!=null)
	// 	{
	// 		m_CharacterSystem.UpgradeSoldier();
	// 	}
	// }
	// //增加Soldier
	// public void AddSoldier(ISoldier theSoldier)
	// {
	// 	if(m_CharacterSystem!=null)
	// 	{
	// 		m_CharacterSystem.AddSodier(theSoldier);
	// 	}
	// }
	// //移除Soldier
	// public void RemoveSoldier(ISoldier theSoldier)
	// {
	// 	if(m_CharacterSystem!=null)
	// 	{
	// 		m_CharacterSystem.RemoveSodier(theSoldier);
	// 	}
	// }

	// //增加Enemy
	// public void AddEnemy(IEnemy theEnemy)
	// {
	// 	if(m_CharacterSystem!=null)
	// 	{
	// 		m_CharacterSystem.AddEnemy(theEnemy);
	// 	}
	// }

	// //移除Enemy
	// public void RemoveEnemy(IEnemy theEnemy)
	// {
	// 	if(m_CharacterSystem!=null)
	// 	{
	// 		m_CharacterSystem.RemoveEnemy(theEnemy);
	// 	}
	// }
	// // 顯示兵營資訊
	// public void ShowCampInfo( ICamp Camp )
	// {
	// 	m_CampInfoUI.ShowInfo( Camp );
	// 	m_SoldierInfoUI.Hide();
	// }

	// // 顯示Soldier資訊
	// public void ShowSoldierInfo( ISoldier Soldier )
	// {
	// 	m_SoldierInfoUI.ShowInfo( Soldier );
	// 	m_CampInfoUI.Hide();
	// }
	// 註冊遊戲事件
	// public void RegisterGameEvent( ENUM_GameEvent emGameEvent, IGameEventObserver Observer)
	// {
	// 	m_GameEventSystem.RegisterObserver( emGameEvent , Observer );
	// }

	// // 通知遊戲事件
	// public void NotifyGameEvent( ENUM_GameEvent emGameEvent, System.Object Param )
	// {
	// 	m_GameEventSystem.NotifySubject( emGameEvent, Param);
	// }


}
