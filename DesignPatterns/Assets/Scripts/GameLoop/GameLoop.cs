﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameLoop : MonoBehaviour
{
    //場景狀態
    SceneStateController m_ScenenStateController=new SceneStateController();
    private void Awake() {
        //轉換場景不會被刪除
        
            GameObject.DontDestroyOnLoad(this.gameObject);
            
        
        //亂數種子
        //UnityEngine.Random.seed=(int)DataTime.Now.Ticks;

    }
    private static bool IsSpawn;
    private bool startOnce=false;
    //Use this for Initialization
    
  
    private void Start() {
        m_ScenenStateController.SetState(new StartState(m_ScenenStateController),"");
    }
    private void Update() {
        m_ScenenStateController.StateUpdate();
    }
}
