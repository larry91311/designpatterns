﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameFunction
{
    public void Update()
    {
        //更新遊戲功能
    }
}
public class GameLoopSimple : MonoBehaviour
{
    GameFunction m_GameFuction=new GameFunction();
    private void Awake() {
        //切換場景不會被刪除
        GameObject.DontDestroyOnLoad(this.gameObject);
    }
    private void Start() {
        //遊戲初始
        //GameInit();
    }

    private void Update() {
        //玩家控制
        //UserInput();

        //遊戲邏輯更新
        //UpdateGameLogic();
        m_GameFuction.Update();

        //畫面更新，由Unity3D負責
    }
}
